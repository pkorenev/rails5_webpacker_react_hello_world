import PropTypes from 'prop-types';
import React from 'react';

export default class Square extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: null,
        };
    }

    clickHandler() {
        //this.setState({value: 'X'})
        this.props.onClick()
    }
    render() {
        return (
            <button className="square" onClick={this.clickHandler.bind(this)}>
                {this.props.value}
            </button>
        );
    }
}