import ReactOnRails from 'react-on-rails';

//import React from 'react';

//import ReactDOM from 'react-dom';

import Game from '../bundles/Game/components/Game';

// This is how react_on_rails can see the HelloWorld in the browser.
ReactOnRails.register({
    Game,
});

//ReactDOM.render(React.createElement(Game, null), document.getElementById('root'));